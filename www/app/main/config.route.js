(function() {
    'use strict';

    angular
        .module('tingm.main')
        .run(appRun);

    appRun.$inject = ['router'];

    function appRun(router) {
        router.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                stateName: 'main',
                stateConfig: {
                    url: '/',
                    views: {
                        'main-view': {
                            templateUrl: 'app/main/main.html',
                            controller: 'MainController as vm'
                        }
                    },
                    data: {
                        'requireLogin': true
                    }
                }
            }
        ];
    }
})();
