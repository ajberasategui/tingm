angular.module('tingm.auth')

.run(appRun);

appRun.$inject = ['$rootScope', '$state', '$ionicModal', '$ionicPopup', 'authService'];

function appRun($rootScope, $state, $ionicModal, $ionicPopup, authService) {

    $rootScope.$on('$stateChangeStart', verifyUser);

    $rootScope.user = {
        "username": "",
        "password": ""
    };

    function verifyUser(event, toState, toParams) {
        if (angular.isDefined(toState.data)) {
            var requiresLogin = toState.data.requireLogin;
            if (requiresLogin && $rootScope.token === undefined) {
                showLogin();
            }
        }
    }

    function showLogin() {
        $rootScope.tgModalData = {
            includeSrc:
                "app/widgets/tg-login-modal.html"
        };

        $ionicModal.fromTemplateUrl(
            'app/widgets/tg-modal.html', {
                scope: $rootScope,
                animation: 'slide-in-up'
            }
        ).then(function(modal) {
            $rootScope.loginModal = modal;
            $rootScope.loginModal.show();
        });
    }

    function closePopup() {
        $rootScope.popup.close();
    }

    function loginComplete(response) {
        if ("error" == response.status) {
            $rootScope.tgPopup = {
                message: response.error,
                buttonLabel: "Ok!",
                close: closePopup
            };
            $rootScope.user = {
                username: "",
                password: ""
            };
            $rootScope.popup = $ionicPopup.alert({
                title: "Login Error",
                templateUrl: "app/widgets/tg-popup.html",
                scope: $rootScope,
                buttons: []
            });
        } else {
            $rootScope.isLoggedIn = true;
            $rootScope.loginModal.hide();
            $state.go('main');
        }
    }

    $rootScope.login = function(form) {
        authService.login($rootScope.user.username, $rootScope.user.password)
            .then(loginComplete);
    };
}
