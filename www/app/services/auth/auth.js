(function() {
    'use strict';

    angular
        .module('tingm.auth')
        .factory('authService', authService);

    authService.$inject = ['$http', '$q', 'tgConfig', '$rootScope'];

    function authService($http, $q, tgConfig, $rootScope, $ionicModal) {
        var token;

        var service = {
            login: login,
            getToken: getToken,
            getUserId: getUserId
        };

        return service;

        function getToken() {
            return $rootScope.token;
        }

        function getUserId() {
            return $rootScope.userId;
        }

        function login(username, password) {
            return $http.get(
                    tgConfig.apiHost + "/v1/web/login/?username=" +
                    username + "&password=" + password)
                .then(loginComplete)
                .catch(loginFailed);
        }

        function loginComplete(response) {
            if ("success" == response.data.status) {
                $rootScope.token = response.data.info.token;
                $rootScope.userId = response.data.info.user_id;
                console.log(response.data);
                return {
                    "status": "success",
                    "token": response.data.info.token
                };
            }
        }

        function loginFailed(error) {
            $q.reject(error);
            // return {
            //     "status": "error",
            //     "error": error.data.info.message
            // };
        }
    }
})();
