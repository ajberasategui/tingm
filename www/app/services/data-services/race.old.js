(function() {
    'use strict';

    angular
        .module('tingm')
        .service('Race', Race);

    Race.$inject = ['$http', 'tgConfig'];

    function Race($http, tgConfig) {
        this.races = [];
        this.get = function(name) {
            var _this = this;
            return $http.get(tgConfig.apiHost + '/race')
                        .then(function(response) {
                            _this.races = response.data;
                            return response.data;
                         });
        };

        this.getByName = function(name) {
            var races = this.races;
            for (var race in races) {
                if (races.hasOwnProperty(race)) {
                    if (races[race].name == name) {
                        return races[race];
                    }
                }
            }
        };

        this.getSenses = function(race) {
            var senses = race.senses.split(',');
            return senses;
        };

        this.getLanguages = function(race) {
            return race.languages.split(',');
        };

        this.getImmunities = function(race) {
            return race.immunities.split(',');
        };
    }
})();
