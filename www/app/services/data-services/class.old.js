(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .service('classService', classService);

    classService.$inject = ['$http', 'tgConfig'];
    // TODO: Refactor as JP factory.
    function classService($http, tgConfig) {
        this.classes = [];
        this.classesTable = [];
        this.get = function() {
            var _this = this;
            // $http.get('http://localhost:8888/tingmapi/class_table')
            // $http.get('http://www.ajberasategui.com/tingmapi/class_table')
            $http.get(tgConfig.apiHost + '/class_table')
                .then(function(response) {
                    _this.classesTable = response.data;
                }, function(error) {
                    console.log(error);
                });
            return $http.get(tgConfig.apiHost + '/class')
                        .then(function(response) {
                            _this.classes = response.data;
                            return response.data;
                         }, function(error) {
                             console.log(error);
                         });
        };
        this.getById  = function(id) {
            return this.findClassById(id);
        };
        this.findClassById = function(id) {
            for (var index in this.classes) {
                var c = this.classes[index];
                if (c.hasOwnProperty('id')) {
                    if (c.id == id) {
                        return c;
                    }
                }
            }
        };
        this.getModifiers = function(classId, level) {
            var classObj = this.findClassById(classId);
            for (var c in this.classesTable) {
                if (this.classesTable.hasOwnProperty(c) &&
                    this.classesTable[c].name == classObj.name &&
                    this.classesTable[c].level == level)
                {
                    return this.classesTable[c];
                }
            }
        };

        this.getAlignments = function(classId) {
            var classObj = this.findClassById(classId);
            return classObj.alignment.split(', ');
        };

        this.getSkills = function(classId) {

        };

        this.getBaseAttack = function(classId, level) {
            var mods = this.getModifiers(classId, level);
            if (angular.isDefined(mods)) {
                return mods.base_attack_bonus;
            } else {
                return null;
            }
        };
    }
})();
