(function() {
    'use strict';

    angular
        .module('tingm')
        .factory('progService', progService);

    progService.$inject = ['$http', 'tgConfig', '$q'];

    function progService($http, tgConfig, $q) {
        var progressions = [];
        var loaded = false;

        var service = {
            getProgressions: getProgressions,
            getProgressionByName: getProgressionByName
        };

        return service;

        function getProgressions() {
            return $http.get(tgConfig.apiHost + '/progressions', {
                    cache: true
                })
                .then(function(response) {
                    progressions = response.data;
                    loaded = true;
                    return progressions;
                });
        }

        function getBaseAttackByName(progName) {
            progName = progName.toLowerCase();
            return getProgressions().then(function(){
                for (var i = 0; i < progressions.length; i++) {
                    if (progName == progressions[i].name) {
                        return progressions[i];
                    }
                }
            });
        }

        function getProgressionByName(name) {
            return getProgressions().then(function(){
                for (var i = 0; i < progressions.length; i++) {
                    console.log(progressions[i]);
                    if (name == progressions[i].name) {
                        return progressions[i];
                    }
                }
            });
        }
    }
})();
