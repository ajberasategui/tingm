(function() {
    'use strict';

    angular
        .module('tingm.dataDecorators')
        .factory('classDecorator', classDecorator);

    classDecorator.$inject = ['common'];

    function classDecorator(common) {
        var service = {
            decorate: decorate
        };

        return service;

        function decorate(theClass, classLevelMods) {
            var classMods = classLevelMods;
            theClass.getHitDie = getHitDie;
            theClass.getHitDieNum = getHitDieNum;
            theClass.getAlignments = getAlignments;
            theClass.setMods = setMods;

            function getHitDieNum() {
                var hitDie = theClass.hit_die;
                return common.toInt(hitDie.substr(1, hitDie.length));
            }

            function getHitDie() {

            }

            function setMods(classLevelMods) {
                classMods = classLevelMods;
            }

            function getAlignments() {
                return theClass.alignment.split(', ');
            }
        }
    }
})();
