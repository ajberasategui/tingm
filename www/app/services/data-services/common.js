(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('dataServices', dataServices);

    dataServices.$inject = [
        'characterService', 'raceService', 'skillService', 'sizeService',
        'classService', 'inventoryService', 'equipmentService',
        'classModsService'
    ];

    function dataServices(
        characterService, raceService, skillService, sizeService,
        classService, inventoryService, equipmentService, classModsService
    ) {
        var service = {
            characterService: characterService,
            raceService: raceService,
            skillService: skillService,
            sizeService: sizeService,
            classService: classService,
            equipmentService: equipmentService,
            inventoryService: inventoryService,
            classModsService: classModsService
        };

        return service;
    }
})();
