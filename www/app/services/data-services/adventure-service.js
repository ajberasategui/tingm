angular.module('tingm')

.service('Adventure', ['$http', function($http) {
    return {
        get: function(id) {
            if (!angular.isDefined(id)) {
                return $http.get('data/adventures.json');
            } else {
                return $http.get('data/adventures.json')
                            .then(function(response) {
                                var advs = response.data;
                                return advs[0];
                            });
            }
        }
    };
}]);
