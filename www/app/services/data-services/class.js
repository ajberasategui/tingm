(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('classService', classService);

    classService.$inject = ['$http', '$q', 'tgConfig', 'authService', 'logger'];

    function classService($http, $q, tgConfig, authService, logger) {
        var classes = [];
        var classesTable = [];

        var service = {
            getClasses: getClasses,
            getClassesModifiers: getClassesModifiers
        };

        return service;

        function getClasses() {
            logger.debug("Getting classes");
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                '/class?token=' + authService.getToken();
            return $http.get(endpoint, { cache: true })
                .then(getCompleteSuccess)
                .catch(getCompleteError);
        }

        function getCompleteSuccess(response) {
            logger.debug("Getting classes complete");
            classes = response.data;
            return classes;
        }

        function getCompleteError(error) {
            logger.debug("Getting classes error", error);
            return $q.reject(error);
        }

        function getClassesModifiers() {
            return $http.get(tgConfig.apiHost + '/class_table')
                .then(getModsCompleteSuccess)
                .catch(getCompleteError);
        }

        function getModsCompleteSuccess(response) {
            classesTable = response.data;
            return classesTable;
        }
    }
})();
