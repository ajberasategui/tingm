angular.module('tingm')

.service('Formula', ['$http', '$q', function($http, $q) {
    return {
        formulas: undefined,
        get: function() {
            if (!angular.isDefined(this.formulas)) {
                return this.request().then(function(formulas) {
                    this.formulas = formulas
                    return this.formulas;
                });
            } else {
                var deferred = $q.defer();
                $q.resolve(this.formulas);
                return $q.promise;
            }
        },
        request: function() {
            return $http.get('data/formulas.json')
                        .then(function(response) {
                            return response.data;
                        }, function(error) {
                            console.log(error);
                        });
        },
        getStatTotal: function(charStat) {
            var total = charStat.base + charStat.magic +
                charStat.misc + charStat.temp;
            return total;
        },
        getStatModifier: function(stat, statValue) {
            var formula = "";
            if (angular.isDefined(formulas['statModifiers'][stat])) {
                formula = formulas['statModifiers'][stat];
            } else {
                // Formulas is not ready, return 0 meanwhile
                formula = "(x - 10) / 2";
            }
            var values = {
                x: statValue
            };
            var mod = math.eval(formula, values);
            if (0 > mod) {
                mod = math.floor(mod);
            } else {
                mod = math.round(mod);
            }
            return mod;
        },
        getHitpoints: function(hitDie, constitutionMod) {
            var hp = Number(hitDie.substr(1, hitDie.length)) + constitutionMod;
            if (0 >= hp) {
                return 1;
            } else {
                return hp;
            }
        },
        getFullArmor: function(character) {
            var armorClass = character.armorClass;
            var full =
                armorClass.natural + armorClass.armor + armorClass.shield +
                character.dexterityMod;
        }
    };
}]);
