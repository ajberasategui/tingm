(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('sizeService', sizeService);

    sizeService.$inject = ['$http', '$q', 'tgConfig', 'authService', 'logger'];

    function sizeService($http, $q, tgConfig, authService, logger) {
        var sizes = [];
        var service = {
            getSizes: getSizes,
            getSize: getSize,
            getSizeByName: getSizeByName
        };

        return service;

        function getSizes() {
            logger.debug("Getting sizes");
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                "/size?token=" + authService.getToken();
            return $http.get(endpoint, { cache: true })
                .then(getCompleteSuccess)
                .catch(getCompleteError);
        }

        // This block should be in a decorator ---------------//
        function getSize(id) {
            return getSizes()
                .then(function(sizes) {
                    return sizes[id];
                })
                .catch(function(error) {
                    return error;
                });
        }

       function getSizeByName(name) {
           return getSizes()
                .then(function(sizes) {
                    for (var size in sizes) {
                        if (sizes.hasOwnProperty(size)) {
                            if (sizes[size].size == name) {
                                return sizes[size];
                            }
                        }
                    }
                })
                .catch(function(error) {
                    return $q.reject(error);
                });
       }
       // This block should be in a decorator ---------------//
       
        function getCompleteSuccess(response) {
            logger.debug("Getting sizes complete");
            sizes = response.data;
            return sizes;
        }

        function getCompleteError(error) {
            console.log("Error");
            $q.reject(error);
        }
    }
})();
