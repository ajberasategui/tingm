(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('raceService', raceService);

    raceService.$inject = ['$http', '$q', 'tgConfig', 'authService', 'logger'];

    function raceService($http, $q, tgConfig, authService, logger) {
        var races = [];

        var service = {
            getRaces: getRaces
        };

        return service;

        function getRaces() {
            logger.debug("Getting races");
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                '/race?token=' + authService.getToken();
            return $http.get(endpoint, { cache: true })
                .then(getCompleteSuccess)
                .catch(getCompleteError);
        }

        function getCompleteSuccess(response) {
            logger.debug("Getting races complete");
            races = response.data;
            return races;
        }

        function getCompleteError(error) {
            logger.debug("Getting races error");
            $q.reject(error);
        }
    }
})();
