(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('characterService', characterService);

    characterService.$inject = [
        '$q', '$http', 'tgConfig', 'characterDecorator'
    ];

    function characterService($q, $http, tgConfig, characterDecorator) {
        var characters = [];
        var current;

        var service = {
            getCharacters: getCharacters,
            saveCharacter: saveCharacter,
            getCharacter: getCharacter,
            getPlayerCharacters: getPlayerCharacters,
            getCurrent: getCurrent,
            setCurrent: setCurrent,
            save: save,
            newCharacter: newCharacter
        };
        return service;

        function saveCharacter(character) {
            // return $http.post('')
            throw "Not implemented";
        }

        function getCharacter(id) {
            console.log("Getting character");
            var character = characters[id];
            characterDecorator.decorate(character);
            return character;
        }

        function getCharacters() {
            return $http.get('data/characters.json')
                // tgConfig.apiHost+"/v1/web/v1/character/?token="+tokenData.token)
                .then(getCharactersComplete)
                .catch(getCharactersFailed);
        }

        function getCharactersComplete(response) {
            characters = response.data;
            return characters;
        }

        function getCharactersFailed(error) {
            logger.error("Get Characters Failed: ", error);
            return $q.reject(error);

            // return {
            //     "status": "error",
            //     "error": error.data.error
            // };
        }

        function getPlayerCharacters(playerId) {
            console.log("Getting player characters");
            var endpoint =
                tgConfig.apiHost + tgConfig.apiBase +
                '/player/' + playerId + '/character';
            return $http.get(endpoint, {})
                .then(function(result) {
                    characters = result.data;
                    return result.data;
                })
                .catch(function(error) {
                    $q.reject(error);
                });
        }

        function newCharacter() {
            return $http.get(
                'app/services/data-services/data/new-character.json', {
                    cache: true
                })
                .then(function(response) {
                    var char = response.data;
                    characterDecorator.decorate(char);
                    return char;
                });
        }

        function save(id, character) {
            console.log(character);
            var endpoint = tgConfig.apiHost + tgConfig.apiBase + '/player/' +
                character.user_id + '/character/' + id;
            return $http.put(endpoint, character)
                .then(saveCompleteSuccess)
                .catch(saveCompleteError);
        }

        function saveCompleteSuccess(result) {
            console.log(result);
        }

        function saveCompleteError(error) {
            console.log(error);
        }

        function setCurrent(id) {
            current = characters[id];
            current.id = id;
            console.log("Setting current character to: ", current);
            characterDecorator.decorate(current);
        }

        function getCurrent() {
            console.log("Sending current character: ", current);
            return $q.resolve(current);
        }
    }
})();
