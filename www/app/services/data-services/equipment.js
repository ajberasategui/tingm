(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .service('equipmentService', equipmentService);

    equipmentService.$inject = ['$http', '$q', 'tgConfig', 'authService', 'logger'];

    function equipmentService($http, $q, tgConfig, authService, logger) {
        var armors = [];

        var service = {
            getArmor: getArmor
        };

        return service;

        function getArmor() {
            logger.debug("Getting Armors");
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                '/armor/?filter=subcategory%3D!Shields,subcategory%3D!Extras';
            return $http.get(endpoint, { chache: true })
                .then(getArmorCompleteSucess)
                .catch(getArmorCompleteError);
        }

        function getArmorCompleteSucess(response) {
            logger.debug("Getting Armors complete");
            armors = response.data;
            return response.data;
        }

        function getArmorCompleteError(error) {
            logger.debug("Getting Armors error");
            return $q.reject(error);
        }
    }
})();
