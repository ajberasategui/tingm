(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('skillService', skillService);

    skillService.$inject = ['$http', '$q', 'common'];

    function skillService($http, $q, common) {
        var service = {
            getSkills: getSkills
        };

        return service;

        function getSkills() {
            logger.debug("Getting Skills");
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                "/skill?token=" + common.authService.getToken();
            return $http.get(endpoint, { cache: true })
                .then(getCompleteSuccess)
                .catch(getCompleteError);
        }

        function getCompleteSuccess(response) {
            logger.debug("Getting Skills complete");
            var skills = response.data;
            return skills;
        }

        function getCompleteError(error) {
            $q.reject(error);
        }
    }
})();
