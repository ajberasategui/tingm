(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('inventoryService', inventoryService);

    inventoryService.$inject = ['$q', '$http', 'tgConfig'];

    function inventoryService($q, $http, tgConfig) {
        var inventory;

        var service = {
            getInventory: getInventory
        };
        return service;

        function getInventory(id) {
            console.log("Getting inventory " + id);
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                '/inventory/' + id;
            return $http.get(endpoint)
                .then(getInventoryCompleteSuccess)
                .catch(getInventoryCompleteError);
        }

        function getInventoryCompleteSuccess(response) {
            console.log("Getting inventory complete");
            inventory = response.data;
            return inventory;
        }

        function getInventoryCompleteError(error) {
            console.log("Getting inventory error");
            return $q.reject(error);
        }
    }
})();
