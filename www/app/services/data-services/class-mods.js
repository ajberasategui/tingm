(function() {
    'use strict';

    angular
        .module('tingm.dataServices')
        .factory('classModsService', classModsService);

    classModsService.$inject = ['$q', '$http', 'tgConfig', 'authService'];

    function classModsService($q, $http, tgConfig, authService) {
        var service = {
            getClassesMods: getClassesMods
        };

        return service;

        function getClassesMods() {
            var endpoint = tgConfig.apiHost + tgConfig.apiBase +
                '/class_table?token=' + authService.getToken();
            return $http.get(endpoint, { cache: true })
                .then(getCompleteSuccess)
                .catch(getCompleteError);
        }

        function getCompleteSuccess(response) {
            return response.data;
        }

        function getCompleteError(error) {
            return $q.reject(error);
        }
    }
})();
