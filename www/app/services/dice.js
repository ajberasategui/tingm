(function() {
    'use strict';

    angular
        .module('tingm.services')
        .factory('dice', dice);

    dice.$inject = ['common'];

    function dice(common) {
        var service = {
            roll: roll,
            rollMulti: rollMulti
        };

        return service;

        function roll(faces) {
            return math.round(math.random(1, faces));
        }

        /**
         * @ngdoc function
         * @name rollMulti
         * @description This function simulates the roll of multiple dices.
         * The dices can be of different faces count and each type of dice
         * can be rolled more than once. The result return is the sum of
         * each roll.
         *
         *
         * @param timesByFaces array containing objects where key is times to
         * roll and value is number of faces of the dice to roll.
         *
         * @return int Sum of dices rolls.
         */
        function rollMulti(timesByFaces) {
            var results = 0;
            for (var i = 0; i < timesByFaces.length; i++) {
                var dice = timesByFaces[i];
                for (var times in dice) {
                    var rolls = common.toInt(times);
                    if (dice.hasOwnProperty(times)) {
                        for (var j = 0; i < rolls; i++) {
                            results += roll(dice[times]);
                        }
                    }
                }
            }
            return results;
        }
    }
})();
