(function() {
    'use strict';

    angular
        .module('tingm', [
            'ionic',
            'pascalprecht.translate',
            'ngStorage',
            // Own Modules
            // Blocks
            'blocks.router',
            'blocks.logger',
            'ajbcommon',
            // TinGM Modules
            'tingm.core',
            'tingm.main',
            'tingm.player',
            'tingm.character',
            'tingm.directives',
            // Services
            'tingm.auth',
            'tingm.services',
            'tingm.dataServices',
            'tingm.dataDecorators'

        ]);
})();
