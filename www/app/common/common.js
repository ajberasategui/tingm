(function() {
    'use strict';

    angular
        .module('ajbcommon')
        .factory('common', common);

    common.$inject = [
        'logger', 'authService', '$ionicPopover', '$ionicPopup',
        '$rootScope', '$ionicModal', '$ionicLoading'];

    function common(
        logger, authService, $ionicPopover, $ionicPopup,
        $rootScope, $ionicModal, $ionicLoading
    ) {
        var popup;

        var service = {
            authService: authService,
            logger: logger,
            toInt: toInt,
            popover: $ionicPopover,
            showPopup: showPopup,
            modal: $ionicModal,
            loading: $ionicLoading
        };

        return service;

        function toInt(num) {
            var toNum = ('number' == typeof(num)) ? num :
                Number.parseInt(num);

            return toNum;
        }

        function showPopup(title, message) {
            $rootScope.tgPopup = {
                'message': message,
                'buttonLabel': 'Ok!',
                'close': closePopup
            };
            popup = $ionicPopup.show({
                templateUrl: 'app/widgets/tg-popup.html',
                title: 'Information',
                scope: $rootScope,
                buttons: []
            });
        }

        function closePopup() {
            popup.close();
            delete $rootScope.tgPopup;
        }
    }
})();
