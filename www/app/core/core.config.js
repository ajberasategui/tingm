
var core = angular.module('tingm.core');


var tgConfig = {
    // "apiHost": "http://www.ajberasategui.com",
    "apiHost": "http://192.168.0.103:8888/tingmapi",
    "apiBase": "/v1/web/v1",
    "env": "local" // "remote"
};

core.constant('tgConfig', tgConfig);

core.config(function($urlRouterProvider, $stateProvider, routerConfigProvider) {
    routerConfigProvider.config.$stateProvider = $stateProvider;
    $urlRouterProvider.otherwise('/');
});
