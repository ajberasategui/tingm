(function() {
    'use strict';

    angular
        .module('blocks.router')
        .provider('routerConfig', routerConfig)
        .factory('router', router);

    router.$inject = ['routerConfig'];

    function routerConfig() {
        /* jshint validthis: true */
        this.config = {
            // Los clientes deben setear los siguientes atributos:
            // $stateProvider: $stateProvider
        };

        this.$get = function() {
            return {
                config: this.config
            };
        };
    }

    function router(routerConfig) {
        var states = [];

        var $stateProvider = routerConfig.config.$stateProvider;

        var service = {
            configureStates: configureStates
        };

        return service;

        function configureStates(states) {
            states.forEach(function(state) {
                $stateProvider.state(state.stateName, state.stateConfig);
            });
        }

        // function getStates() {
        //
        // }

    }


})();
