(function() {
    'use strict';

    angular
        .module('tingm.player')
        .run(appRun);

    appRun.$inject = ['router'];

    function appRun(router) {
        router.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                stateName: 'playerMain',
                stateConfig: {
                    url: '/playermain',
                    views: {
                        'main-view': {
                            templateUrl: 'app/player/main.html',
                            controller: 'PlayerController as vm'
                        }
                    },
                    resolve: {
                        data: function(playerResolve) {
                            return playerResolve.resolve();
                        },
                    },
                    data: {
                        'requireLogin': true
                    }
                }
            }
        ];
    }
})();
