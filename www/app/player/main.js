(function() {
    'use strict';

    angular
        .module('tingm.player')
        .controller('PlayerController', PlayerController);

    PlayerController.$inject = ['data', 'characterService'];

    function PlayerController(data, characterService) {
        var vm = this;

        vm.setCurrentCharacter = setCurrentCharacter;

        activate();

        function activate() {
            vm.characters = data.characters;
            vm.races = data.races;
            vm.classes = data.classes;
        }

        function setCurrentCharacter(id) {
            characterService.setCurrent(id);
        }
    }
})();
