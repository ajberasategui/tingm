(function() {
    'use strict';

    angular
        .module('tingm.character')
        .factory('playerResolve', playerResolve);

    playerResolve.$inject = [
        '$q', 'characterService', 'raceService', 'classService', 'common'
    ];

    function playerResolve(
        $q, characterService, raceService, classService, common
    ) {
        var service = {
            resolve: resolve
        };

        return service;

        function resolve() {
            var userId = common.authService.getUserId();
            var promises = [
                characterService.getPlayerCharacters(userId),
                raceService.getRaces(),
                classService.getClasses()
            ];

            return $q.all(promises)
                .then(function(results) {
                    return {
                        characters: results[0],
                        races: results[1],
                        classes: results[2]
                    };
                })
                .catch(function(errors) {
                    return $q.reject(errors);
                });
        }
    }
})();
