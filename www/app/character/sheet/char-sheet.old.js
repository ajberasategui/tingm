(function() {
    'use strict';

    angular
        .module('tingm.character')
        .directive('tgCharSheet', tgCharSheet);

    function tgCharSheet() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-char-sheet/tg-char-sheet.html',
            scope: {
                characterId: '@characterId',
                edit: '@edit'
            },
            link: linkFunc,
            controller: Controller,
            controllerAs: 'vm'
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = [
        '$scope',
        '$state',
        'dice',
        'raceService',
        'classService',
        'characterService',
        // 'Equipment',
        // 'Size',
        '$ionicPopover',
        '$ionicLoading',
        '$ionicModal'
    ];

    function Controller(
        $scope,
        $state,
        dice,
        raceService,
        classService,
        characterService,
        // Equipment,
        // Size,
        $ionicPopover,
        $ionicLoading,
        $ionicModal
    ) {
        var vm = this;

        activate();

        function activate() {

            vm.classesReady = false;
            vm.racesReady = false;
            vm.characterReady = false;
            vm.equipmentReady = false;
            vm.characterReady = false;
            vm.sizeReady = false;

            $scope.collapsedCards = ['ac', 'saves'];
            $scope.visibleCard = 'stat';

            $scope.alignments = [];

            classService.getClasses().then(function(result) {
                $scope.classes = result;
                vm.classesReady = true;
                isLoaded();
            });

            raceService.getRaces().then(function(result) {
                $scope.races = result;
                vm.racesReady = true;
                isLoaded();
            });

            // Equipment.loadArmor().then(function(result) {
            //     $scope.armors = result;
            //     vm.equipmentReady = true;
            //     isLoaded();
            // });
            //
            // Size.get().then(function(result) {
            //     $scope.sizes = result;
            //     vm.sizeReady = true;
            //     isLoaded();
            // });

            characterService.getCharacters()
                .then(function(response) {
                    vm.characterReady = true;
                    var characters = response.data;
                    $scope.character = {}; //new Character(characters[-1]);
                    isLoaded();
                }).catch(function(error) {
                    throw {
                        "errorCode": "no-token"
                    };
                });

            $ionicPopover.fromTemplateUrl(
                'templates/popovers/sum-popover.html', {
                    scope: $scope
            }).then(function(pop) {
                $scope.sumPopover = pop;
            });
        }

        function isLoaded() {
            // $ionicLoading.hide();
            if (
                vm.classesReady &&
                vm.racesReady &&
                vm.equipmentReady
            ) {
                $ionicLoading.hide();
            } else {
                console.log("Classes Ready: " + vm.classesReady);
                console.log("Races Ready: " + vm.racesReady);
                console.log("Equipment Ready: " + vm.equipmentReady);
            }
        }

        $scope.$on("save", function(event, args) {
            saveCharacter();
        });

        function saveCharacter() {
            characterService.saveCharacter($scope.character);
        }

        $scope.levelUp = function() {
            // TODO: Show level up modal!
            $scope.character.levelUp();

        };

        $scope.levelDown = function() {
            // TODO: Show level up modal!
        };

        // $scope.raceChanged = function() {
        //     $scope.race = Race.getByName($scope.character.race);
        //     $scope.character.updateRace($scope.race);
        // };

        $scope.showSumPopover = function(
            $event,
            stat,
            title,
            diceAttr,
            type
        ) {
            var statObj = {};
            var depth = stat.split('.');
            if (1 < depth.length) {
                statObj = $scope.character[depth[0]];
                for (var i = 1; i < depth.length; i++) {
                    statObj = statObj[depth[i]];
                }
            } else {
                statObj = $scope.character[stat];
            }
            $scope.sumPopover.data = {
                "attrName": stat,
                "title": title,
                "statObj": statObj,
                "diceAttr": diceAttr,
                "type": type
            };
            $scope.sumPopover.show($event);
        };

        $scope.closePopover = function() {
            // TODO: Como hacer cuando no es un stat, si no algo como el armor.

            // throw new Error("Implementar correctamente");
            switch($scope.sumPopover.data.type) {
                case 'stat':
                    $scope.statChanged($scope.sumPopover.data.attrName);
                    break;
                case 'armor':
                    $scope.armorChanged();
                    break;
                case 'initiative':
                    $scope.initiativeChanged();
                    break;
                case 'saves':
                    savingChanged($scope.sumPopover.data.attrName);
                    break;
            }
            $scope.sumPopover.hide();
        };

        function savingChanged(type) {
            var path = type.split('.');
            $scope.character.updateSaving(path[1]);
        }

        $scope.initiativeChanged = function() {
            $scope.character.updateInitiative();
        };

        $scope.classChanged = function() {
            $scope.character.setClass($scope.charClass);
            $scope.alignments = Class.getAlignments($scope.charClass);
        };

        $scope.isCardCollapsed = function(cardName) {
            return -1 != $scope.collapsedCards.indexOf(cardName);
        };

        $scope.toggleCard = function(cardName) {
            if ($scope.isCardCollapsed(cardName)) {
                $scope.collapsedCards.push($scope.visibleCard);
                var idx = $scope.collapsedCards.indexOf(cardName);
                $scope.visibleCard =
                    $scope.collapsedCards.splice(idx, 1)[0];
            }
        };

        $scope.closeModal = function() {
            $scope.tgModal.hide();
        };

        $scope.showModal = function(type) {
            var includeSrc = "templates/partials/";
            switch (type) {
                case 'race':
                    includeSrc += "tg-race-info.html";
                    $scope.race = Race.getByName($scope.character.race);
                    break;
                case 'class':
                    includeSrc += "tg-class-info.html";
                    $scope.currClass =
                        Class.getById($scope.character.theclasses[0]);
                    break;
                case 'armor':
                    includeSrc += "tg-armor-info.html";
                    break;
            }
            $ionicModal.fromTemplateUrl(
                'templates/partials/tg-modal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }
            ).then(function(modal) {
                $scope.tgModal = modal;
                $scope.tgModalData = {
                    includeSrc: includeSrc,
                    close: $scope.closeModal
                };
                $scope.tgModal.show();
            });
        };

        $scope.statChanged = function(stat) {
            var statPath = stat.split('.');
            $scope.character.updateStat(statPath[1]);
        };

        $scope.armorChanged = function() {
            $scope.character.updateTotalArmor();
        };

        $scope.rollDice = function(faces, applyTo, type, stat) {
            var roll = Dice.roll(faces);
            if (angular.isDefined(applyTo)) {
                eval("$scope." + applyTo + " = " + roll);
            }
            if ("stat" == type) {
                $scope.statChanged(stat);
            }
            return roll;
        };

        $scope.armorEquipChanged = function() {
            $scope.armor =
                Equipment.getArmorById($scope.charEquipment.armorId);
            $scope.character.updateArmorClass($scope.armor);
        };
    }
})();
