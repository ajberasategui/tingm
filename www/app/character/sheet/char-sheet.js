(function() {
    'use strict';

    angular
        .module('tingm.character')
        .controller('CharSheetController', CharSheetController);

    CharSheetController.$inject = [
        '$scope', 'data', 'edit', 'classDecorator',
        'dice', 'common', '$state', 'characterService'
    ];

    function CharSheetController(
        $scope, data, edit, classDecorator,
        dice, common, $state, characterService
    ) {
        var vm = this;

        activate();

        function activate() {

            // Data coming from view resolve
            vm.edit = edit;
            $scope.edit = edit;

            vm.races = data.races;
            vm.sizes = data.sizes;
            vm.classMods = data.classMods;
            vm.armors = data.armors;
            console.log(vm.armors);

            vm.character = data.character;
            vm.characterId = data.characterId;

            // TODO: Multi class
            vm.classes = data.classes;

            vm.collapsedCards = ['ac', 'saves'];
            vm.visibleCard = 'stat';

            vm.minLevel = 1;
            vm.maxLevel = 20;

            // TODO: Should be improved to multiple classes / level per class
            vm.charClass = [];
            vm.alignments = [];

            if (-1 != vm.characterId) {
                loadViewModelData();
            }
        }

        // ---- ViewModel methods ---- //
        vm.levelUp = levelUp;
        vm.levelChange = levelChange;
        vm.raceChanged = raceChanged;
        vm.classChanged = classChanged;
        vm.statChanged = statChanged;
        vm.armorChanged = armorChanged;
        vm.initiativeChanged = initiativeChanged;
        vm.classAdd = classAdd;

        vm.isCardCollapsed = isCardCollapsed;
        vm.toggleCard = toggleCard;

        vm.toInt = toInt;

        vm.showModal = showModal;
        vm.showSumPopover = showSumPopover;
        vm.save = save;
        vm.cancel = cancel;

        // ---- Elements that MUST be on scope instead of vm ---- //
        $scope.rollDice = rollDice;

        // ---- Implementations ---- //
        function toInt(string) {
            return parseInt(string, 10);
        }

        function loadViewModelData() {
            vm.character.race_id = toInt(vm.character.race_id);
            vm.alignments = vm.classes[vm.character.theclasses[0]].alignment
                .split(', ');
        }

        function classAdd() {
            console.log(vm.character.theclasses);
            console.log('nothing');
        }

        function levelUp() {
            console.log("nothing");
            // TODO: By level hp roll editor.
            // TODO: Ask for dice roll for hp update.
            // TODO: Set level input to "number with apply" control.
            // TODO: Change class mods to corresponding level.
        }

        function levelChange() {
            if (0 === vm.character.theclasses.length) {
                // TODO: Show popoup: "You must choose your class first"
                common.showPopup(
                    'Information',
                    'Please choose your class first.'
                );
            } else {
                var level = vm.character.level;
                if (1 < level) {
                    for (var i = 2; i <= level; i++) {
                        if (angular.isUndefined(vm.character.level_data[i])) {
                            vm.character.level_data[i] = {
                                "hit_die_result": 0,
                                "choosen_abs": null,
                                "choosen_feat": null,
                                "choosen_stat": null
                            };
                        }
                    }
                }
                vm.showModal('levelChange');
            }
        }

        function raceChanged() {
            vm.race = vm.races[vm.character.race_id];
            vm.character.updateRace(vm.race, vm.sizes[vm.race.size]);
        }

        function classChanged() {
            console.log('TODO: Multiclass');
            var theClass = vm.character.theclasses[0];
            var classLevel =
                vm.classes[theClass].name + '_' + vm.character.level;

            // TODO: Right now passing an array of only 1 element for both
            // parameters. Should pass all classes and mods by level for
            // multi-character
            var classWithMods = {};
            classWithMods[theClass] = vm.classMods[classLevel];
            var classObj = vm.classes[theClass];
            classDecorator.decorate(classObj, vm.classMods[classLevel]);
            vm.alignments = classObj.getAlignments();
            vm.character.updateClass([classObj],[classWithMods]);
        }

        function isCardCollapsed(cardName) {
            return -1 != vm.collapsedCards.indexOf(cardName);
        }

        function toggleCard(cardName) {
            if (vm.isCardCollapsed(cardName)) {
                vm.collapsedCards.push(vm.visibleCard);
                var idx = vm.collapsedCards.indexOf(cardName);
                vm.visibleCard =
                    vm.collapsedCards.splice(idx, 1)[0];
            }
        }

        function statChanged(stat) {
            var statPath = stat.split('.');
            vm.character.updateStat(statPath[1]);
        }

        function initiativeChanged() {
            vm.character.updateInitiative();
        }

        function showModal(type) {
            var includeSrc = 'app/character/sheet/modals/';
            $scope.tgModalData = {};
            switch (type) {
                case 'levelChange':
                    includeSrc += "level-change.html";
                    $scope.tgModalData.title = 'Level Change';
                    $scope.tgModalData.levelInfo = {
                        level: vm.character.level,
                        levelData: vm.character.level_data
                    };
                    break;
                case 'race':
                    includeSrc += "race-info.html";
                    $scope.race = Race.getByName($scope.character.race);
                    break;
                case 'class':
                    includeSrc += "class-info.html";
                    $scope.currClass =
                        Class.getById($scope.character.theclasses[0]);
                    break;
                case 'armor':
                    includeSrc += "armor-info.html";
                    break;
            }
            common.modal.fromTemplateUrl('app/widgets/tg-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                vm.tgModal = modal;
                $scope.tgModalData.includeSrc = includeSrc;
                $scope.tgModalData.close = closeModal;
                vm.tgModal.show();
            });
        }

        function closeModal() {
            vm.tgModal.hide();
        }

        function showSumPopover($event, stat, title, diceAttr, type) {
            common.popover.fromTemplateUrl(
                'app/widgets/sum-popover.html', {
                    scope: $scope
            }).then(function(pop) {
                vm.sumPopover = pop;
                var statObj = {};
                var depth = stat.split('.');
                if (1 < depth.length) {
                    statObj = vm.character[depth[0]];
                    for (var i = 1; i < depth.length; i++) {
                        statObj = statObj[depth[i]];
                    }
                } else {
                    statObj = vm.character[stat];
                }
                $scope.sumPopoverData = {
                    "attrName": stat,
                    "title": title,
                    "statObj": statObj,
                    "diceAttr": diceAttr,
                    "type": type,
                    "close": closePopover
                };
                vm.sumPopover.show($event);
            });

        }

        function closePopover() {
            // TODO: Como hacer cuando no es un stat, si no algo como el armor.
            // throw new Error("Implementar correctamente");
            switch($scope.sumPopoverData.type) {
                case 'stat':
                    vm.statChanged($scope.sumPopoverData.attrName);
                    break;
                case 'armor':
                    vm.armorChanged($scope.sumPopoverData.attrName);
                    break;
                case 'initiative':
                    vm.initiativeChanged();
                    break;
            }
            vm.sumPopover.hide();
        }

        function rollDice(faces, applyTo) {
            common.logger.warning("Roll Dice TODO: Multi dice roll");
            var result = dice.roll(faces);
            applyValueTo(result, applyTo);
        }

        function applyValueTo(value, applyTo) {
            var path = applyTo.split('.');
            var attr = vm[path[0]];

            for (var i = 1; i < path.length - 1; i++) {
                attr = attr[path[i]];
            }
            attr[path[path.length - 1]] = value;
        }

        function armorChanged(armorType) {
            var path = armorType.split('.');
            vm.character.updateArmor(path[path.length - 1]);
        }

        function save() {
            characterService.save(vm.characterId, vm.character);
        }

        function cancel() {
            console.log('TODO: Alert of not saving');
            $state.go('playerMain');
        }
    }
})();
