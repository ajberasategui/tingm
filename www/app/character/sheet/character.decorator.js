(function() {
    'use strict';

    angular
        .module('tingm.character')
        .factory('characterDecorator', characterDecorator);

    characterDecorator.$inject = ['common'];

    function characterDecorator(common) {

        var service = {
            decorate: decorate
        };

        return service;

        function decorate(character) {
            var statModFormula = "(x - 10) / 2";
            var currentClass;

            character.updateRace = updateRace;
            character.updateClass = updateClass;
            character.updateAttackBonus = updateAttackBonus;
            character.updateSkillpoints = updateSkillpoints;
            character.updateHide = updateHide;
            character.updateArmor = updateArmor;
            character.updateStat = updateStat;
            character.updateArmor = updateArmor;
            character.updateInitiative = updateInitiative;

            /**
             * @ngdoc function
             * @name updateRace
             * @description sets the character race_id to the one passed
             * as parameter.
             * Updates every state related to race:
             * - Character Size
             * - Grapple by Size
             * - Hide by Size
             * - Skillpoints bonus
             * - Senses, languages and immunities
             * - Race stats
             * - Armor (related to dexMod)
             */
            function updateRace(newRace, newSize) {
                character.size_name = newSize.size;
                console.log(character.size_name);
                // Grapple
                character.attack_bonus.grapple.size =
                    common.toInt(newSize.grapple_mod);
                // Hide
                character.hide.size = common.toInt(newSize.hide_mod);
                character.updateHide();
                // Skillpoints
                character.skillpoints.race = newRace.bonus_sp;
                character.updateSkillpoints();
                // Attack
                character.updateAttackBonus('grapple');
                // Senses
                character.senses.racial = newRace.senses;
                // Languages
                character.languages.racial = newRace.languages;
                // Immunities
                character.immunities.racial = newRace.immunities;
                // Stats
                var stats = [
                    'strenght', 'dexterity', 'constitution',
                    'intelligence', 'wisdom', 'charisma'
                ];
                for (var stat in stats) {
                    if (stats.hasOwnProperty(stat)) {
                        var statName = stats[stat];
                        if ("" !== newRace[statName]) {
                            character.stats[statName].racial =
                                common.toInt(newRace[statName]);
                        } else {
                            character.stats[statName].racial = 0;
                        }
                        character.updateStat(statName);
                    }
                }
            }

            /**
             * @ngdoc function
             * @name
             * @description
             *
             * @param array of character classes objects
             * @param classesWithMods array(for future multi-class) containing
             * objects { classId : classLevelMods }
             */
            function updateClass(classes, classesWithMods) {
                // TODO: This must be redone to support multi-classes
                currentClass = classes[0];

                var classAndMods = classesWithMods[0];
                var classId = Object.keys(classAndMods)[0];
                var mods = classAndMods[classId];
                character.theclasses.push(classId);
                updateBaseAttack(mods.base_attack_bonus);

                var savings = character.savings;
                savings.fortitude.base = common.toInt(mods.fort_save);
                savings.reflex.base = common.toInt(mods.ref_save);
                savings.will.base = common.toInt(mods.will_save);
                updateSavings();

                var theClass = classes[0];//[character.theclasses[0]];
                updateHitpoints(theClass);
                if (null === character.level_data) {
                    character.level_data = {
                        1: {}
                    };
                }
                character.level_data[1].hit_die_result = character.hitpoints;
            }

            function updateSavings() {
                ['fortitude', 'reflex', 'will'].forEach(updateSaving);
            }

            function updateSaving(type) {
                var saving = character.savings[type];
                saving.total = 0;
                for (var attr in saving) {
                    if (saving.hasOwnProperty(attr) && 'total' != attr) {
                        saving.total += saving[attr];
                    }
                }
            }

            function updateBaseAttack(baseAttackBonus) {
                var byAttack = baseAttackBonus.split('/');
                character.base_attack = {};
                for (var i = 0; i < byAttack.length; i++) {
                    character.base_attack['base'+(i+1)] = byAttack[i];
                }
                updateAttackBonus('grapple');
                updateAttackBonus('melee');
                updateAttackBonus('ranged');
            }

            function updateHitpoints() {
                var constMod = character.stats.constitutionMod;
                var hp = 1;
                if (angular.isDefined(currentClass)) {
                    hp = currentClass.getHitDieNum() + constMod;
                } else {
                    hp = constMod;
                }
                hp = (0 <= hp) ? hp : 1;
                character.hitpoints = hp;
                character.curr_hitpoints = hp;
            }

            function updateAttackBonus(type) {
                var bonus = character.attack_bonus[type];
                if (angular.isUndefined(bonus)) {
                    bonus = {};
                }
                var baseAttack = character.base_attack;
                var mod;
                switch (type) {
                    case 'grapple':
                    case 'melee':
                        mod = character.stats.strenghtMod;
                        break;
                    case 'ranged':
                        mod = character.stats.dexterityMod;
                        break;
                }
                // Set totals to base totals for each hit
                for (var base in baseAttack) {
                    if (
                        baseAttack.hasOwnProperty(base) &&
                        null !== baseAttack[base]
                    ) {
                        var num = base.substring(base.length - 1);
                        bonus['total'+num] = common.toInt(baseAttack[base]) +
                            common.toInt(mod);
                        for (var idx in bonus) {
                            var isTotal = idx.indexOf("total") != -1;
                            if (bonus.hasOwnProperty(idx) && !isTotal) {
                                bonus['total'+num] += bonus[idx];
                            }
                        }
                    }
                }
            }

            function updateSkillpoints() {
                var sp = character.skillpoints;
                sp.total = 0;
                for (var idx in sp) {
                    if (sp.hasOwnProperty(idx) && 'total' != sp) {
                        sp.total += common.toInt(sp[idx]);
                    }
                }
            }

            function updateHide() {
                var hide = character.hide;
                hide.total = 0;
                for (var idx in hide) {
                    if (hide.hasOwnProperty(idx) && 'total' != idx) {
                        hide.total += common.toInt(hide[idx]);
                    }
                }
            }

            /**
             * @ngdoc function
             * @name updateArmor
             * @description
             *
             * @param type full, flat or touch
             */
            function updateArmor(type) {
                updateBaseArmor();
                switch (type) {
                    case 'full':
                        updateFullArmor();
                        break;
                    case 'flat':
                        updateFlatFootedArmor();
                        break;
                    case 'touch':
                        updateTouchArmor();
                        break;
                }

            }

            function updateFullArmor() {
                var armorClass = character.armor_class;
                armorClass.full.total = armorClass.natural +
                    armorClass.full.temp + armorClass.full.misc +
                    character.stats.dexterityMod;
            }

            function updateFlatFootedArmor() {
                var armorClass = character.armor_class;
                armorClass.flatFooted.total = armorClass.natural +
                    armorClass.flatFooted.misc + armorClass.flatFooted.temp;
            }

            function updateTouchArmor() {
                var armorClass = character.armor_class;
                armorClass.touch.total = armorClass.natural +
                    armorClass.touch.misc + armorClass.touch.temp -
                    (armorClass.shield + armorClass.armor);
                console.log(armorClass.touch.total);
            }

            function updateBaseArmor() {
                var armorClass = character.armor_class;
                armorClass.base = 0;
                for (var idx in armorClass) {
                    if (armorClass.hasOwnProperty(idx)) {
                        if ('object' != typeof(armorClass[idx])) {
                            armorClass.base += common.toInt(armorClass[idx]);
                        }
                    }
                }
            }

            function updateStat(statName) {
                var stat = character.stats[statName];
                stat.total = 0;
                for (var idx in stat) {
                    if (stat.hasOwnProperty(idx) && 'total' != idx) {
                        stat.total += common.toInt(stat[idx]);
                    }
                }
                var values = { x: stat.total };
                var mod = math.eval(statModFormula, values);
                mod = (mod > 0) ? math.floor(mod) : mod = math.round(mod);
                character.stats[statName + 'Mod'] = mod;

                switch (statName) {
                    case 'constitution':
                        updateConstitution();
                        break;
                    case 'dexterity':
                        updateDexterity();
                        break;
                    case 'strenght':
                        updateStrenght();
                        break;
                    case 'wisdom':
                        updateWisdom();
                        break;
                }
            }

            function updateWisdom() {
                character.savings.will.ability =
                    character.stats.wisdomMod;
                updateSaving('will');
            }

            function updateStrenght() {
                updateAttackBonus('grapple');
                updateAttackBonus('melee');
            }

            function updateConstitution() {
                updateHitpoints();
                character.savings.fortitude.ability =
                    character.stats.constitutionMod;
                updateSaving('fortitude');
            }

            function updateDexterity() {
                updateArmor('full');
                updateArmor('touch');
                updateArmor('flat');
                updateAttackBonus('ranged');
                updateInitiative();
                character.savings.reflex.ability =
                    character.stats.dexterityMod;
                updateSaving('reflex');
            }

            /**
             * @ngdoc function
             * @name updateInitiative
             * @description updates total initiative suming every initiative
             * modifier plus the dexterity modifier.
             */
            function updateInitiative() {
                var initiative = character.initiative;
                initiative.total = initiative.magic + initiative.misc +
                    initiative.temp + character.stats.dexterityMod;
            }
        }
    }
})();
