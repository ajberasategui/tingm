(function() {
    'use strict';

    angular
        .module('tingm.character')
        .factory('charSheetResolve', charSheetResolve);

    charSheetResolve.$inject = ['$q', 'dataServices'];

    function charSheetResolve($q, dataServices) {
        var service = {
            resolve: resolve
        };

        return service;

        function resolve(charId) {
            var charPromise = (-1 == charId) ?
                dataServices.characterService.newCharacter()
                : dataServices.characterService.getCurrent();
            var promises = [
                dataServices.raceService.getRaces(),
                dataServices.classService.getClasses(),
                dataServices.sizeService.getSizes(),
                dataServices.classModsService.getClassesMods(),
                charPromise,
                dataServices.equipmentService.getArmor()
            ];

            return $q.all(promises)
                .then(function(results) {
                    var sizesByName = {};
                    for (var size in results[2]) {
                        if (results[2].hasOwnProperty(size)) {
                            var name = results[2][size].size;
                            sizesByName[name] = results[2][size];
                        }
                    }
                    var classModsByClassAndLevel = {};
                    for (var idx in results[3]) {
                        if (results[3].hasOwnProperty(idx)) {
                            var classLevel = results[3][idx];
                            var n = classLevel.name + "_" + classLevel.level;
                            classLevel.id = idx;
                            classModsByClassAndLevel[n] = classLevel;
                        }
                    }
                    console.log("Resolving everything");
                    return {
                        races: results[0],
                        classes: results[1],
                        sizes: sizesByName,
                        classMods: classModsByClassAndLevel,
                        character: results[4],
                        characterId: charId,
                        armors: results[5]
                    };
                })
                .catch(function(errors) {
                    console.log("Resolving Errors", errors);
                    return $q.reject(errors);
                });
        }
    }
})();
