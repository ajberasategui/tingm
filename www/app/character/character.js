(function() {
    'use strict';

    angular
        .module('tingm.character')
        .controller('CharacterController', CharacterController);

    CharacterController.$inject = ['viewConfig'];

    function CharacterController(viewConfig) {
        var vm = this;

        vm.save = save;
        
        activate();

        function activate() {

            vm.characterId = viewConfig.characterId;
            vm.edit = viewConfig.edit;
            vm.viewTitle = viewConfig.viewTitle;
        }

        function save() {
            $scope.$broadcast("save", "algo");
        }

        // classService.getClasses().then(function(classes) {
        //     vm.classes = classes;
        // });
    }
})();
