(function() {
    'use strict';

    angular
        .module('tingm.main')
        .run(appRun);

    appRun.$inject = ['router'];

    function appRun(router) {
        router.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                stateName: 'playerMain.character',
                stateConfig: {
                    url: '/character',
                    abstract: true,
                    data: {
                        'requireLogin': true
                    }
                }
            }, {
                stateName: 'playerMain.character.view',
                stateConfig: {
                    url: '/view/{id}',
                    views: {
                        'main-view@': {
                            templateUrl: 'app/sheet/sheet.html',
                            resolve: {
                                viewConfig: function() {
                                    return {
                                        edit: false,
                                        viewTitle: "Char Name TODO",
                                        characterId: id
                                    };
                                }
                            },
                            controller: 'SheetController as vm'
                        }
                    }
                }
            }, {
                stateName: 'playerMain.character.edit',
                stateConfig: {
                    url: '/edit/:charId',
                    abstract: true,
                    views: {
                        'main-view@': {
                            templateUrl: 'app/character/character.html',
                            resolve: {
                                viewConfig: function($stateParams) {
                                    return {
                                        edit: true,
                                        characterId: $stateParams.charId,
                                        viewTitle: "Character"
                                    };
                                }
                            },
                            controller: "CharacterController as vm"
                        }
                    }
                }
            }, {
                stateName: 'playerMain.character.edit.sheet',
                stateConfig: {
                    url: '/sheet',
                    views: {
                        'sheet-tab': {
                            templateUrl: 'app/character/sheet/char-sheet.html',
                            controller: 'CharSheetController as vm',
                            resolve: {
                                data: function(charSheetResolve, $stateParams)
                                {
                                    return charSheetResolve.resolve(
                                        $stateParams.charId
                                    );
                                },
                                edit: function() {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }, {
                stateName: 'playerMain.character.edit.skills',
                stateConfig: {
                    url: '/skills',
                    views: {
                        'skills-tab': {
                            templateUrl: 'app/character/skills/skills.html',
                            controller: 'SkillsController as vm',
                            resolve: {
                                data: function(skillsResolve) {
                                    return skillsResolve.resolve();
                                },
                                edit: function() {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }, {
                stateName: 'playerMain.character.edit.inventory',
                stateConfig: {
                    url: '/inventory',
                    views: {
                        'inventory-tab': {
                            templateUrl: 'app/character/inventory/inventory.html',
                            controller: 'InventoryController as vm',
                            resolve: {
                                data: function(inventoryResolve) {
                                    return inventoryResolve.resolve();
                                },
                                edit: function() {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        ];
    }
})();
