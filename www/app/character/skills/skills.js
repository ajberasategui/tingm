(function() {
    'use strict';

    angular
        .module('tingm.character')
        .controller('SkillsController', SkillsController);

    SkillsController.$inject = ['common', 'data'];

    function SkillsController(common, data) {
        var vm = this;

        activate();

        function activate() {
            vm.skills = data;
        }
    }
})();
