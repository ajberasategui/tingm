(function() {
    'use strict';

    angular
        .module('tingm.character')
        .factory('skillsResolve', skillsResolve);

    skillsResolve.$inject = ['$q', 'skillService'];

    function skillsResolve($q, skillService) {
        var service = {
            resolve: resolve
        };

        return service;

        function resolve() {
            var promises = [
                skillService.getSkills()
            ];

            return $q.all(promises)
                .then(function(results) {
                    return results[0];
                })
                .catch(function(errors) {
                    $q.reject(errors);
                });
        }
    }
})();
