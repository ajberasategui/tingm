(function() {
    'use strict';

    angular
        .module('tingm.character')
        .factory('inventoryResolve', inventoryResolve);

    inventoryResolve.$inject = ['$q', 'dataServices'];

    function inventoryResolve($q, dataServices) {
        var service = {
            resolve: resolve
        };

        return service;

        function resolve() {
            return dataServices.characterService.getCurrent()
                .then(function(character) {
                    var promises = [
                        dataServices.inventoryService.getInventory(
                            character.inventory_id
                        )
                    ];

                    return $q.all(promises)
                        .then(function(results) {
                            console.log("Inventory results: ", results);
                            return {
                                inventory: results[0]
                            };
                        })
                        .catch(function(errors) {
                            console.log("Get inv error: ", errors);
                            return $q.reject(errors);
                        });
                })
                .catch(function(error) {
                    console.log("No current character set");
                    console.log(error);
                });

        }
    }
})();
