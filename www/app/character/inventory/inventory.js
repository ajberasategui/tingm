(function() {
    'use strict';

    angular
        .module('tingm.character')
        .controller('InventoryController', InventoryController);

    InventoryController.$inject = ['data'];

    function InventoryController(data) {
        var vm = this;

        activate();

        // ---- View Model assignments ---- //

        // ---- View Model functions implementations ---- //


        // ---- Private functions implementations ---- //
        function activate() {
            vm.inventory = data.inventory;
            vm.items = vm.inventory.items;
        }
    }
})();
