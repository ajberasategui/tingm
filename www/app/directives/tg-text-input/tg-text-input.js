(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgTextInput', tgTextInput);

    function tgTextInput() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-text-input/tg-text-input.html',
            scope: {
                tgValue: "=tgValue",
                tgName: "@tgName",
                tgModel: "=tgModel",
                tgRequired: "=tgRequired",
                tgMinlength: "=tgMinlength",
                tgType: "@tgType",
                placeholder: "@placeholder"
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = ['$scope'];

    function Controller($scope) {
        activate();

        function activate() {

        }
    }
})();
