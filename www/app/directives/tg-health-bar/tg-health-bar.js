(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgHealthBar', tgHealthBar);

    function tgHealthBar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-health-bar/tg-health-bar.html',
            scope: {
                hitpoints: "=tgHitpoints",
                currHitpoints: "=tgCurrHitpoints"
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = ['$scope'];

    function Controller($scope) {
        activate();

        function activate() {

        }

        $scope.healthPercentage = function() {
            var hp = $scope.currHitpoints * 100
                / $scope.hitpoints;
            return hp;
        };
    }
})();
