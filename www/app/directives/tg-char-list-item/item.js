(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('characterItem', characterItem);

    function characterItem() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-char-list-item/item.html',
            scope: {
                character: '=',
                theclass: '=',
                race: '='
            },
            link: linkFunc
        };

        return directive;

        function linkFunc() {
        }
    }
})();
