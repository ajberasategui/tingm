(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgCharPortrait', tgCharPortrait);

    function tgCharPortrait() {
        var directive = {
            restrict: 'E',
            templateUrl:
                'app/directives/tg-char-portrait/tg-char-portrait.html',
            scope: {
                portraitUrl: "=tgPortraitUrl"
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = ['$scope'];

    function Controller($scope) {
        activate();

        function activate() {

        }
    }
})();
