(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgButton', tgButton);

    function tgButton() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-button/tg-button.html',
            scope: {
                tgLabel: "@tgLabel",
                tgDisabled: "=tgDisabled",
                tgType: "@tgType"
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {

        }
    }

    Controller.$inject = ['$scope'];

    function Controller($scope) {
        activate();

        function activate() {

        }
    }
})();
