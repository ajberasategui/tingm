(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgNumberInput', tgNumberInput);

    function tgNumberInput() {
        var directive = {
            require: ['ngModel'],
            restrict: 'E',
            templateUrl: 'app/directives/tg-number-input/tg-number-input.html',
            scope: {
                ngModel: "=",
                disabled: "=",
                tgSpacing: "@",
                tgMin: "=",
                tgMax: "=",
                onInc: "&tgOnInc",
                onDec: "&tgOnDec",
                tgApply: '&'
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attrs, ctrls) {

            scope.incrementDisabled = false;
            scope.decrementDisabled = false;

            var ngModelCtrl = ctrls[0];

            scope.hasApply = !angular.isUndefined(attrs.tgApply);

            // We watch for ngModel until is set to its initial value
            // and we can compare to min and max to disable buttons if
            // necessary. Once the value is set and the buttons were
            // set as corresponding, the watch is removed.
            var unregister = scope.$watch('ngModel', function(val) {
                if (!angular.isUndefined(val)) {
                    if (scope.ngModel == scope.tgMin) {
                        scope.decrementDisabled = true;
                    }

                    if (scope.ngModel == scope.tgMax) {
                        scope.incrementDisabled = true;
                    }
                    unregister();
                }
            });

            ngModelCtrl.$viewChangeListeners.push(function() {
                scope.$eval(attrs.ngChange);
            });

            if (angular.isUndefined(scope.tgSpacing)) {
                scope.tgSpacing = 0;
            }
            if (angular.isUndefined(scope.tgMin)) {
                scope.tgMin = "none";
            }
            if (angular.isUndefined(scope.tgMax)) {
                scope.tgMax = "none";
            }

            scope.apply = function() {
                scope.tgApply();
            }

            scope.increment = function() {
                scope.onInc();
                if ("none" != scope.tgMax) {
                    if (scope.ngModel < scope.tgMax) {
                        scope.ngModel++;
                        if (scope.decrementDisabled &&
                            scope.ngModel > scope.tgMin
                        ) {
                            scope.decrementDisabled = false;
                        }
                    }
                    if (scope.ngModel == scope.tgMax) {
                        scope.incrementDisabled = true;
                    }
                } else {
                    scope.ngModel++;
                }
                ngModelCtrl.$setViewValue(scope.ngModel);
            };

            scope.decrement = function() {
                scope.onDec();
                if ("none" != scope.tgMin) {
                    if (scope.ngModel > scope.tgMin) {
                        scope.ngModel--;
                        if (scope.incrementDisabled &&
                            scope.ngModel < scope.tgMax
                        ) {
                            scope.incrementDisabled = false;
                        }
                    }
                    if (scope.ngModel == scope.tgMin) {
                        scope.decrementDisabled = true;
                    }
                } else {
                    scope.ngModel--;
                }
                ngModelCtrl.$setViewValue(scope.ngModel);
            };
        }
    }

    Controller.$inject = [];

    function Controller($scope) {

        activate();

        function activate() {
        }
    }
})();
