(function() {
    'use strict';

    angular
        .module('tingm.directives')
        .directive('tgSelectMultiple', tgSelectMultiple);

    function tgSelectMultiple() {

        var directive = {
            restrict: 'E',
            templateUrl: 'app/directives/tg-select-multiple/tg-select-multiple.html',
            scope: {
                tgContainer: '=',
                tgOptions: '=',
                tgLabel: '@',
                tgClick: '&'
            },
            link: linkFunc,
            controller: Controller
        };

        return directive;

        function linkFunc(scope, el, attr, ctrl) {
            scope.current = null;

            scope.choosen = [];

            scope.addItem = function() {
                var id = scope.current;
                var newClass = scope.tgOptions[scope.current];
                newClass.id = id;
                scope.choosen.push(newClass);
            };

            scope.removeItem = function() {
                scope.choosen.splice(scope.toRemove, 1);
            };
        }
    }

    Controller.$inject = [];

    function Controller() {
        var vm = this;

        activate();

        function activate() {

        }
    }
})();
